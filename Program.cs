﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace TModLoaderInstaller
{
	public static class Program
	{
		public const string Windows = "Windows";
		public const string Linux = "Linux";
		public const string Mac = "Mac";
		public const string InstallationFilesDirectory = "InstallationFiles";

		public static string OS;

		private static void Main()
		{
			Console.Title = "tModLoader Installer";

			Console.WriteLine("Preparing installation...");

			Configuration.Initialize();

			if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
				OS = Windows;
			} else if(RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) {
				OS = Mac;
			} else {
				OS = Linux; //Is Linux the best default?
			}

			try {
				string terrariaDirectory = Paths.GetTerrariaDirectory();

				Utils.WriteLineColor($"tModLoader will be installed to '{terrariaDirectory}'.",ConsoleColor.Green);

				BackupFiles(terrariaDirectory);
				CopyFiles(terrariaDirectory);

				Utils.WriteLineColor("\r\nSuccess!\r\n",ConsoleColor.Green);
				Console.WriteLine($"Press any key to close this window.");
			}
			catch(InstallerException e) {
				Utils.WriteLineColor(e.Message,ConsoleColor.Red);
			}
			catch(Exception e) {
				Utils.WriteLineColor($"An unhandled {e.GetType().Name} has occured: '{e.Message}'.\r\n{e.StackTrace}",ConsoleColor.Red);
			}

			Console.ReadKey();
		}

		private static void BackupFiles(string terrariaDirectory)
		{
			if(OS!=Windows) {
				Console.WriteLine($"Skipping backups, not required on {OS}.");
				return;
			}

			const string CurrentBinaryName = "Terraria.exe";

			string VanillaBackupName = $"Terraria_v{Configuration.Get("GameVersion")}.exe";

			string currentBinaryPath = Path.Combine(terrariaDirectory,CurrentBinaryName);
			string vanillaBackupPath = Path.Combine(terrariaDirectory,VanillaBackupName);

			bool gog = !File.Exists(Path.Combine(terrariaDirectory,Configuration.Get(OS,"SteamCheckFile")));

			string vanillaHash = gog ? Configuration.Get(OS,"VanillaHashGOG") : Configuration.Get(OS,"VanillaHashSteam");
			string currentHash = Utils.GetFileMD5Hash(currentBinaryPath);

			var currentBinaryFile = new FileInfo(currentBinaryPath);

			bool currentIsVanilla = currentBinaryFile.Exists && currentHash==vanillaHash;

			if(currentIsVanilla) {
				Utils.WriteLineColor($"Detected Vanilla. Backing up '{CurrentBinaryName}' to '{VanillaBackupName}'.",ConsoleColor.Green);

				File.Copy(currentBinaryPath,vanillaBackupPath,true);
			}
		}
		private static void CopyFiles(string terrariaDirectory)
		{
			var filesDirectory = Path.GetFullPath(InstallationFilesDirectory);

			if(!Directory.Exists(filesDirectory)) {
				throw new InstallerException($"No installation files found! Did you separate the installer from its '{InstallationFilesDirectory}' directory?");
			}

			var files = Directory.GetFiles(filesDirectory);

			if(files.Length==0) {
				throw new InstallerException($"No installation files found! The '{InstallationFilesDirectory}' directory is empty.");
			}

			Utils.WriteLineColor("Copying files...",ConsoleColor.Green);

			for(int i = 0;i<files.Length;i++) {
				string sourceFile = files[i];
				string relativePath = Path.GetRelativePath(filesDirectory,sourceFile);
				string destinationFile = Path.Combine(terrariaDirectory,relativePath);

				Console.WriteLine($"\tCopying '{Path.GetFileName(sourceFile)}'...");

				File.Copy(sourceFile,destinationFile,true);
			}
		}
	}
}

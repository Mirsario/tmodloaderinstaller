﻿using System;

namespace TModLoaderInstaller
{
	public class InstallerException : Exception
	{
		public InstallerException(string message) : base(message) {}
	}
}

﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TModLoaderInstaller
{
	public static class Configuration
	{
		private static readonly Regex ConfigRegex = new Regex(@"([\S]+)[^\S\r\n]*=[^\S\r\n]*([^\r\n]+)",RegexOptions.Compiled);

		private static Dictionary<string,string> values;

		internal static void Initialize()
		{
			string configString = Properties.Resources.Installer;

			values = new Dictionary<string,string>();

			var matches = ConfigRegex.Matches(configString);

			foreach(Match match in matches) {
				string key = match.Groups[1].Value;
				string value = match.Groups[2].Value;

				if(value!=null) {
					value = value.Trim();

					if(value!=null && value[0]=='"' && value[^1]=='"') {
						value = value.Length>2 ? value[1..^1] : null;
					}

					if(string.IsNullOrWhiteSpace(value)) {
						value = null;
					}
				}

				values[key] = value;
			}
		}

		public static string Get(string key) => values[key];
		public static string Get(string key,string subKey) => values[$"{key}.{subKey}"];
		public static string Get(params string[] keys) => values[string.Join('.',keys)];
	}
}

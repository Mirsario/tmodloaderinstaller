﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace TModLoaderInstaller
{
	public static class Paths
	{
		public const int TerrariaAppId = 105600;

		public readonly static string TerrariaManifestFile = $"appmanifest_{TerrariaAppId}.acf";

		private readonly static Regex SteamLibraryFoldersRegex = new Regex(@"""(\d+)""[^\S\r\n]+""(.+)""",RegexOptions.Compiled);
		private readonly static Regex SteamManifestInstallDirRegex = new Regex(@"""installdir""[^\S\r\n]+""([^\r\n]+)""", RegexOptions.Compiled);

		public static string GetTerrariaDirectory()
		{
			static bool ConfirmDirectory(string directory) => Directory.Exists(directory) && Directory.Exists(Path.Combine(directory,"Content"));

			if(TryGetSteamDirectory(out string steamDirectory) && TryGetTerrariaDirectoryFromSteam(steamDirectory,out string terrariaDirectory)) {
				return terrariaDirectory;
			}

			Utils.WriteLineColor("Unable to automatically find Terraria's installation path.",ConsoleColor.DarkYellow);

			string path = null;

			do {
				if(path!=null) {
					Utils.WriteLineColor("Invalid installation path. Try again.",ConsoleColor.Red);
				}

				Console.WriteLine("Please type out Terraria's installation path manually: ");

				path = Console.ReadLine();
			}
			while(!ConfirmDirectory(path));

			return path;
		}
		public static bool TryGetTerrariaDirectoryFromSteam(string steamDirectory,out string path)
		{
			string steamApps = Path.Combine(steamDirectory,"steamapps");

			var libraries = new List<string>() {
				steamApps
			};

			string libraryFoldersFile = Path.Combine(steamApps,"libraryfolders.vdf");

			if(File.Exists(libraryFoldersFile)) {
				string contents = File.ReadAllText(libraryFoldersFile);

				var matches = SteamLibraryFoldersRegex.Matches(contents);

				foreach(Match match in matches) {
					string directory = Path.Combine(match.Groups[2].Value.Replace(@"\\",@"\"),"steamapps");

					if(Directory.Exists(directory)) {
						libraries.Add(directory);
					}
				}
			}

			for(int i = 0;i<libraries.Count;i++) {
				string directory = libraries[i];
				string manifestPath = Path.Combine(directory,TerrariaManifestFile);

				if(File.Exists(manifestPath)) {
					string contents = File.ReadAllText(manifestPath);
					var match = SteamManifestInstallDirRegex.Match(contents);

					if(match.Success) {
						path = Path.Combine(directory,"common",match.Groups[1].Value);

						if(Directory.Exists(path)) {
							return true;
						}
					}
				}
			}

			path = null;

			return false;
		}
		public static bool TryGetSteamDirectory(out string path)
		{
#if WINDOWS
			string keyPath = Environment.Is64BitOperatingSystem ? @"SOFTWARE\Wow6432Node\Valve\Steam" : @"SOFTWARE\Valve\Steam";

			using RegistryKey key = Registry.LocalMachine.CreateSubKey(keyPath);

			path = key.GetValue("InstallPath") as string;
#elif LINUX
			path = "~/.local/share/Steam";
#elif MAC
			path = "~/Library/Application Support/Steam";
#else
			path = null;
#endif

			return path!=null && Directory.Exists(path);
		}
	}
}

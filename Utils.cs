﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace TModLoaderInstaller
{
	public static class Utils
	{
		public static void WriteLineColor(string text,ConsoleColor color)
		{
			Console.ForegroundColor = color;

			Console.WriteLine(text);

			Console.ResetColor();
		}
		public static string GetFileMD5Hash(string path)
		{
			using var md5 = MD5.Create();
			using var stream = File.OpenRead(path);

			return BytesToHex(md5.ComputeHash(stream),false);
		}
		public static string BytesToHex(byte[] bytes,bool upperCase)
		{
			StringBuilder result = new StringBuilder(bytes.Length*2);

			for(int i = 0;i < bytes.Length;i++) {
				result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));
			}

			return result.ToString();
		}
	}
}
